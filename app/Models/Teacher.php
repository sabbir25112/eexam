<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Teacher extends Model
{
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function subjectStudent()
    {
        return $this->hasManyThrough(SubjectStudent::class, SubjectTeacher::class);
    }

    public function subjectTeacher()
    {
        return $this->hasMany(SubjectTeacher::class);
    }
}

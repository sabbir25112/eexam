<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    protected $guarded = ['id'];

    public function subjectTeacher()
    {
        return $this->belongsTo(SubjectTeacher::class);
    }
}

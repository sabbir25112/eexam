<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubjectTeacher extends Model
{
    protected $guarded = ['id'];
    protected $table = "subject_teacher";

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }

    public function subjectStudent()
    {
        return $this->hasMany(SubjectStudent::class);
    }

    public function enrollRequest()
    {
        return $this->hasMany(EnrollRequest::class);
    }

    public function exams()
    {
        return $this->hasMany(Exam::class);
    }
}

<?php namespace App\Models;

use App\Models\SubjectTeacher;
use Illuminate\Database\Eloquent\Model;

class SubjectStudent extends Model
{
    protected $guarded = ['id'];
    
    public function subjectTeacher()
    {
        return $this->belongsTo(SubjectTeacher::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}

<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $guarded = ['id'];

    public function teacher()
    {
        return $this->belongsToMany(Teacher::class);
    }

    public function subjectTeacher()
    {
        return $this->hasMany(SubjectTeacher::class);
    }

    public function subjectStudent()
    {
        return $this->hasManyThrough(SubjectStudent::class, SubjectTeacher::class);
    }
}

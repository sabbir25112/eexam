<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Exam extends Model
{
    protected $guarded = ['id'];

    public function subjectTeacher()
    {
        return $this->belongsTo(SubjectTeacher::class);
    }

    public function questions()
    {
        return $this->hasMany(ExamQuestion::class);
    }

    public function students()
    {
        return $this->hasMany(ExamStudent::class);
    }

    public function isEligibleNow(Student $student)
    {
        $student_exam = $this->students()->where('student_id', $student->id)->first();
        if ($student_exam) {
            $created_before = $student_exam->created_at->diffInMinutes(Carbon::now());
            return $created_before <= $this->time;
        }
        return true;
    }

    public function scopePublished($query)
    {
        return $query->where('is_publish', true);
    }

    public function createQuestionAnswer(Student $student)
    {
        $student_exam = $this->students()->where('student_id', $student->id)->first();
        if ($student_exam) return $student_exam;
        return $this->students()->create([
            'exam_id' => $this->id, 
            'student_id' => $student->id
        ]);
    }

    public function calculateMark(Student $student)
    {
        $qustions = $this->questions;
        $mark = 0;

        foreach($qustions as $question) {
            $answer = $question->answers()->where('student_id', $student->id)->first();
            $mark += $answer->mark;
        }
        return $mark;
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\SubjectStudent;
use Illuminate\Http\Request;

class SubjectStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubjectStudent  $subjectStudent
     * @return \Illuminate\Http\Response
     */
    public function show(SubjectStudent $subjectStudent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SubjectStudent  $subjectStudent
     * @return \Illuminate\Http\Response
     */
    public function edit(SubjectStudent $subjectStudent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SubjectStudent  $subjectStudent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubjectStudent $subjectStudent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubjectStudent  $subjectStudent
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubjectStudent $subjectStudent)
    {
        //
    }
}

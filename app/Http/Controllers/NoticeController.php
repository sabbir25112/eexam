<?php namespace App\Http\Controllers;

use App\Models\Notice;
use Illuminate\Http\Request;

class NoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->isTeacher()) {
            $teacher = auth()->user()->teacher;
            $notices = Notice::whereHas('subjectTeacher', function($query) use ($teacher) {
                return $query->where('teacher_id', $teacher->id);
            })->with('subjectTeacher.subject')->get();

            return teacherView('notice.index', compact('notices'));
        }
        return redirect('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->isTeacher()) {
            $teacher = auth()->user()->teacher;
            $teacher->load('subjectTeacher.subject');
            return teacherView('notice.create', compact('teacher'));
        }
        return redirect('home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'subject_teacher_id' => 'required',
            'message' => 'required'
        ]);
        if (auth()->user()->isTeacher()) {
            Notice::create($request->all());
            return redirect()->route('notice.index');
        }
        return redirect('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function show(Notice $notice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function edit(Notice $notice)
    {
        if (auth()->user()->isTeacher()) {
            $teacher = auth()->user()->teacher;
            $teacher->load('subjectTeacher.subject');
            return teacherView('notice.edit', compact('teacher', 'notice'));
        }
        return redirect('home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notice $notice)
    {
        $this->validate($request, [
            'title' => 'required',
            'subject_teacher_id' => 'required',
            'message' => 'required'
        ]);

        if (auth()->user()->isTeacher()) {
            $notice->update($request->all());
            return redirect()->route('notice.index');
        }
        return redirect('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notice $notice)
    {
        //
    }
}

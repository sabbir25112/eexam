<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Notice;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->isStudent()) {
            $student = auth()->user()->student;
            $notices = Notice::with('subjectTeacher.subjectStudent', 'subjectTeacher.subject')->get();
            $notices = $notices->reject(function ($notice) use ($student) {
                $student_count_is_zero = $notice->subjectTeacher->subjectStudent->count() == 0;
                if ($student_count_is_zero) return true;
                $current_user_not_found = $notice->subjectTeacher->subjectStudent->filter(function ($subjectStudent)  use ($student) {
                    return $student->id == $subjectStudent->student_id;
                });
                if ($current_user_not_found) return false;
                return true;
            });
            return studentView('home', compact('notices'));
        }
        elseif (Auth::user()->isAdmin()) return adminView('home');
        elseif (Auth::user()->isTeacher()) return teacherView('home');
    }
}

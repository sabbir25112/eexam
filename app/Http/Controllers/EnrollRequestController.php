<?php

namespace App\Http\Controllers;

use App\Models\EnrollRequest;
use App\Models\SubjectStudent;
use Illuminate\Http\Request;

class EnrollRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->isStudent()) {
            $student = auth()->user()->student;
            $pending_requests = EnrollRequest::where('student_id', $student->id)
                ->Status(constants('ENROLL_STATUS')['Pending'])
                ->with('subjectTeacher.subject', 'subjectTeacher.teacher.user')->paginate(10);
            return studentView('subject.pending_enroll_request', compact('pending_requests'));
        } elseif (auth()->user()->isTeacher()) {
            $teacher = auth()->user()->teacher;
            $pending_requests = EnrollRequest::whereHas('subjectTeacher', function($query) use ($teacher) {
                return $query->where('teacher_id', $teacher->id);
            })->Status(constants('ENROLL_STATUS')['Pending'])
            ->with('student.user')
            ->paginate(10);
            return teacherView('subject.pending_enroll_request', compact('pending_requests'));
        }
        return redirect('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'subject' => 'required',
            'subject_teacher' => 'required'
        ]);
        $student = auth()->user()->student;
        $pending_request = EnrollRequest::where(['subject_teacher_id' => $request->subject_teacher, 'student_id' => $student->id])->count();
        if($pending_request) return redirect()->back();
        EnrollRequest::create(['subject_teacher_id' => $request->subject_teacher, 'student_id' => $student->id]);
        return redirect()->route('enroll-request.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EnrollRequest  $enrollRequest
     * @return \Illuminate\Http\Response
     */
    public function show(EnrollRequest $enrollRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EnrollRequest  $enrollRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(EnrollRequest $enrollRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EnrollRequest  $enrollRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EnrollRequest $enrollRequest)
    {
        if (auth()->user()->isTeacher()) {
            $enrollRequest->update([
                'status' => $request->status
            ]);
            if ($request->status == constants('ENROLL_STATUS')['Accepted']) {
                SubjectStudent::create([
                    'subject_teacher_id' => $enrollRequest->subject_teacher_id,
                    'student_id' => $enrollRequest->student_id,
                ]);
            }
            return redirect()->back();
        } 
        return redirect('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EnrollRequest  $enrollRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(EnrollRequest $enrollRequest)
    {
        //
    }
}

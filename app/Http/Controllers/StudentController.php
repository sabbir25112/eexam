<?php namespace App\Http\Controllers;

use TSC\Repositories\StudentRepository;
use Illuminate\Http\Request;
use App\Models\Student;
class StudentController extends Controller
{
    private $studentRepository;

    public function __construct(StudentRepository $studentRepository)
    {
        $this->studentRepository = $studentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::with(['user'])->withCount(['subjectStudent', 'examStudent'])->paginate(10);
        return adminView('student.index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        $student->load(['user', 'subjectStudent.subjectTeacher.subject', 'subjectStudent.subjectTeacher.teacher.user', 'examStudent.exam.subjectTeacher.subject']);
        return adminView('student.show', compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        if (auth()->user()->isStudent()) {
            if (auth()->user()->student->id != $student->id) return redirect('home');
            $student->load('user');
            return studentView('profile-edit', compact('student'));
        }

        return redirect('home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        if (auth()->user()->isStudent() && auth()->user()->student->id == $student->id) {
            $rules = [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,id,'.$student->id,
                'student_id' => 'required|string|max:255|unique:users,id,'.$student->id,
            ];
            if (!is_null($request->password)) {
                $rules['password'] = 'required|string|min:6|confirmed';
            }
    
            $this->validate($request, $rules);
    
            $data = [
                'name' => $request->name, 
                'email' => $request->email,
                'student_id' => $request->student_id
            ];
            if (!is_null($request->password)) {
                $data['password'] = bcrypt($request->password);
            }
            $student->user->update($data);
        }

        return redirect('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        //
    }
}

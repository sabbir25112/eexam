<?php namespace App\Http\Controllers;

use App\Models\Subject;
use App\Models\Teacher;
use App\Models\SubjectStudent;
use App\Models\SubjectTeacher;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->isAdmin()) {
            $subjects = Subject::withCount('teacher')->paginate(10);
            return adminView('subject.index', compact('subjects'));
        } elseif (auth()->user()->isStudent()) {
            $subjects = SubjectStudent::with('subjectTeacher.subject', 'subjectTeacher.teacher.user')->paginate(10);            
            return studentView('subject.index', compact('subjects'));
        } elseif (auth()->user()->isTeacher()) {
            $teacher = auth()->user()->teacher;
            $subjects = $teacher->load(['subjectTeacher' => function($query) {
                return $query->with('subject')->withCount('subjectStudent');
            }])->subjectTeacher;
            return teacherView('subject.index', compact('subjects'));
        }
        
        return redirect('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return adminView('subject.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:6',
            'code' => 'required|unique:subjects|min:4'
        ]);
        $subject = Subject::create([
            'name' => $request->name,
            'code' => $request->code
        ]);
        return redirect()->route('subject.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        if (auth()->user()->isAdmin()) {
            return $this->showForAdmin($subject);
        }
        return redirect('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject)
    {
        return adminView('subject.edit', compact('subject'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subject $subject)
    {
        $this->validate($request, [
            'name' => 'required|string|min:6',
            'code' => "required|unique:subjects,code,$subject->id|min:4"
        ]);
        $subject->update([
            'name' => $request->name,
            'code' => $request->code
        ]);
        return redirect()->route('subject.index');
    }

    public function assignTeacher(Subject $subject, Request $request)
    {
        if ($request->has('teacher')) {
            $subject->teacher()->sync($request->teacher);
            return 1;
        }
        return 0;
    }

    private function showForAdmin(Subject $subject)
    {
        $subject = $subject->load(['teacher' => function ($query) {
            return $query->withCount('subjectStudent')->with('user');
        }]);
        $teachers = Teacher::with('user')->doesntHave('subjectTeacher', 'and', function($query) use ($subject) {
            return $query->where('subject_id', $subject->id)->toSql();
        })->get();
        return adminView('subject.show', compact('subject', 'teachers'));
    }

    public function unenrolledSubjects()
    {
        $student = auth()->user()->student;
        $subjects = Subject::doesntHave('subjectStudent', 'and', function($query) use ($student) {
            return $query->where('student_id', $student->id);
        })->with('subjectTeacher.teacher.user')->get();
        return studentView('subject.enroll', compact('subjects'));
    }

    public function studentList(SubjectTeacher $subject)
    {
        if (auth()->user()->isTeacher()) {
            $subject->load('subjectStudent.student.user', 'subject');
            return teacherView('subject.student-list', compact('subject'));
        }
        return redirect('home');
    }
}

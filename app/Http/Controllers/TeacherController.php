<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TSC\Repositories\TeacherRepository;
use App\Models\Teacher;
class TeacherController extends Controller
{

    public function __construct(TeacherRepository $teacherRepository) 
    {
        $this->teacherRepository = $teacherRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = $this->teacherRepository->getAllWithUser();
        return adminView('teacher.index', compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return adminView('teacher.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|string'
        ]);

        $teacher = $this->teacherRepository->create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        return redirect()->route('teacher.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        if (auth()->user()->isAdmin()) {
            $teacher->load(['user', 'subjectTeacher' => function ($query) {
                return $query->with(['subject', 'subjectStudent', 'enrollRequest' => function ($query2) {
                    return $query2->status(constants('ENROLL_STATUS')['Pending']);
                }]);
            }]);

            return adminView('teacher.show', compact('teacher'));
        }
        return redirect('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit($teacher)
    {
        $teacher = $this->teacherRepository->findWithUser($teacher);
        return adminView('teacher.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $teacher)
    {
        $teacher = $this->teacherRepository->findWithUser($teacher);
        $this->validate($request, [
            'name' => 'required|string|min:3',
            'email' => "required|email|unique:users,email,".$teacher->user->id.",id",
            'password' => 'confirmed'
        ]);
        
        $data = [
            'name' => $request->name,
            'email' => $request->email,
        ];
        if(!empty($request->password)) $data['password'] = bcrypt($request->password);
        $this->teacherRepository->update($teacher, $data);
        return redirect()->route('teacher.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher)
    {
        //
    }
}

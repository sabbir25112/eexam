<?php

namespace App\Http\Controllers;

use App\Models\ExamQuestion;
use App\Models\Exam;
use App\Models\Option;
use Illuminate\Http\Request;
use DB;
class ExamQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (request()->has('exam_id')) {
            if (auth()->user()->isTeacher()) {
                $exam = Exam::find(request('exam_id'));
                return teacherView('exam.add-question', compact('exam'));
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'exam_id' => 'required|exists:exams,id',
            'question' => 'required|string',
            'option_1' => 'required|string',
            'option_2' => 'required|string',
            'option_3' => 'required|string',
            'option_4' => 'required|string',
            'answer' => 'required|in:1,2,3,4',
            'mark' => 'required|min:1'
        ]);

        DB::transaction(function () use ($request) {
            $exam_question = ExamQuestion::create([
                'question_statement' => $request->question,
                'exam_id' => $request->exam_id,
                'mark' => $request->mark
            ]);

            Option::create([
                'exam_question_id' => $exam_question->id,
                'option_statement' => $request->option_1,
                'option_image' => $request->hasFile('option_image_1') ? $this->addQuestionImage($exam_question->id, 1, $request->file('option_image_1')) : null,
                'is_answer' => $request->answer == 1
            ]);

            Option::create([
                'exam_question_id' => $exam_question->id,
                'option_statement' => $request->option_2,
                'option_image' => $request->hasFile('option_image_2') ? $this->addQuestionImage($exam_question->id, 2, $request->file('option_image_2')) : null,
                'is_answer' => $request->answer == 2
            ]);

            Option::create([
                'exam_question_id' => $exam_question->id,
                'option_statement' => $request->option_3,
                'option_image' => $request->hasFile('option_image_3') ? $this->addQuestionImage($exam_question->id, 3, $request->file('option_image_3')) : null,
                'is_answer' => $request->answer == 3
            ]);

            Option::create([
                'exam_question_id' => $exam_question->id,
                'option_statement' => $request->option_4,
                'option_image' => $request->hasFile('option_image_4') ? $this->addQuestionImage($exam_question->id, 4, $request->file('option_image_4')) : null,
                'is_answer' => $request->answer == 4
            ]);
        });
        return redirect()->route('exam.show', $request->exam_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ExamQuestion  $examQuestion
     * @return \Illuminate\Http\Response
     */
    public function show(ExamQuestion $examQuestion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ExamQuestion  $examQuestion
     * @return \Illuminate\Http\Response
     */
    public function edit(ExamQuestion $examQuestion)
    {
        if (auth()->user()->isStudent()) return redirect('home');
        $examQuestion->load('options');
        return teacherView('exam.edit-question', compact('examQuestion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ExamQuestion  $examQuestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExamQuestion $examQuestion)
    {
        $examQuestion->load('options');
        DB::transaction(function () use ($request, $examQuestion) {
            $examQuestion->update([
                'question_statement' => $request->question,
                'mark' => $request->mark
            ]);

            foreach($examQuestion->options as $count => $option) {
                $data = [
                    'option_statement' => $request["option_".($count+1)],
                    'option_image' => $request->hasFile("option_image_".($count+1)) ? $this->addQuestionImage($examQuestion->id, $count+1, $request->file("option_image_".($count+1))) : $option->option_image,
                    'is_answer' => $request->answer == $count + 1
                ];
                
                $option->update($data);
            }
        });
        return redirect()->route('exam.show', $examQuestion->exam_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ExamQuestion  $examQuestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExamQuestion $examQuestion)
    {
        //
    }

    private function addQuestionImage($exam_question_id, $option_number, $file)
    {
        $fileName = 'option_image_'.$exam_question_id.'_'.$option_number.'.'.$file->getClientOriginalExtension();
        $destinationPath = public_path().'/img/exam_questions/' ;
        $file->move($destinationPath,$fileName);

        return $fileName;
    }
}

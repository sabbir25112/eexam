<?php namespace App\Http\Controllers;

use App\Models\Exam;
use App\Models\QuestionAnswer;
use Illuminate\Http\Request;
use DB;
class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->isTeacher()) {
            $teacher = auth()->user()->teacher;
            $exams = Exam::whereHas('subjectTeacher', function ($query) use ($teacher) {
                return $query->where('teacher_id', $teacher->id);
            })->with('subjectTeacher.subject')->paginate(10);
            return teacherView('exam.index', compact('exams'));
        } elseif (auth()->user()->isStudent()) {
            $student = auth()->user()->student;
            $exams = Exam::published()->whereHas('subjectTeacher', function ($query) use ($student) {
                return $query->whereHas('subjectStudent', function ($query2) use ($student) {
                    return $query2->where('student_id', $student->id);
                });
            })->with([
                'subjectTeacher.subject', 
                'subjectTeacher.teacher.user',
                'students' => function ($query) use ($student) {
                    return $query->where('student_id', $student->id);
                }])->get();
            return studentView('exam.index', compact('exams'));
        }
        return redirect('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->isTeacher()) {
            $teacher = auth()->user()->teacher;
            $subjects = $teacher->load('subjectTeacher.subject')->subjectTeacher;
            
            return teacherView('exam.create', compact('subjects'));
        }
        return redirect('home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (auth()->user()->isTeacher()) {
            $this->validate($request, [
                'name' => 'required',
                'subject' => 'required',
                'time' => 'required'
            ]);
            
            Exam::create([
                'name' => $request->name,
                'subject_teacher_id' => $request->subject,
                'time' => $request->time
            ]);

            return redirect()->route('exam.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function show(Exam $exam)
    {
        if (auth()->user()->isTeacher()) {
            $exam = $exam->load(['questions.options', 'subjectTeacher.subject', 'students']);
            return teacherView('exam.show', compact('exam'));
        } elseif (auth()->user()->isStudent()) {
            $student = auth()->user()->student;
            $exam = $exam->load(['subjectTeacher.subject', 'students' => function($query) use ($student) {
                return $query->where('student_id', $student->id);
            }]);
            return studentView('exam.show', compact('exam', 'student'));
        }
        return redirect('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam $exam)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exam $exam)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exam $exam)
    {
        //
    }

    public function changeStatus(Exam $exam)
    {
        if (auth()->user()->isStudent()) return redirect()->route('home');
        $data = ['is_publish' => !$exam->is_publish];
        $exam->update($data);
        return redirect()->back();
    }

    public function takeExam(Exam $exam)
    {
        if (auth()->user()->isStudent()) {
            $student = auth()->user()->student;
            if ($exam->isEligibleNow($student)) {
                $exam->load('questions.options');
                $exam_student = $exam->createQuestionAnswer($student);
                $exam_end_time = $exam_student->created_at->addMinutes($exam->time);
                return studentView('exam.take-exam', compact('exam', 'exam_student', 'exam_end_time'));
            } 
            return redirect('home');
        }

        return redirect('home');
    }

    public function storeAnswer(Request $request, Exam $exam)
    {
        if (auth()->user()->isStudent()) {
            DB::transaction(function() use ($request, $exam) {
                $student = auth()->user()->student;
                $exam_student = $request->exam_student;
                $questions = $exam->load('questions.options')->questions;
                foreach($questions as $question) {
                    $question_answer = QuestionAnswer::where([
                        'exam_question_id' => $question->id,
                        'student_id' => $student->id
                    ])->first();
                    if (!$request->has('question_answer_'.$question->id) && !$question_answer) {
                        QuestionAnswer::create([
                            'exam_question_id' => $question->id,
                            'student_id' => $student->id
                        ]);
                    } else {
                        $answer = $request['question_answer_'.$question->id];
                        $currect_answer = $question->options->where('is_answer', true)->first()->id;
                        if (!$question_answer) {
                            QuestionAnswer::create([
                                'exam_question_id' => $question->id,
                                'student_id' => $student->id,
                                'option_id' => $answer,
                                'is_currect' => $answer == $currect_answer,
                                'mark' => $answer == $currect_answer ? $question->mark : 0
                            ]);
                        } else {
                            $question_answer->update([
                                'exam_question_id' => $question->id,
                                'student_id' => $student->id,
                                'option_id' => $answer,
                                'is_currect' => $answer == $currect_answer,
                                'mark' => $answer == $currect_answer ? $question->mark : 0
                            ]);
                        }
                    }
                }
            });

            return redirect()->route('exam.show', $exam->id);
        }
        return redirect('home');
    }

    public function student(Exam $exam)
    {
        $exam->load('students.student.user');
        return teacherView('exam.show-student', compact('exam'));
    }
}

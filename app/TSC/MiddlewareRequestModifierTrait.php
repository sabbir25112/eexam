<?php namespace TSC;

trait MiddlewareRequestModifierTrait{
    private function addUserData(&$request)
    {
        if (auth()->user()->isAdmin()) {
            $request->request->add([constants('USER_TYPES')['ADMIN'] => auth()->user()]);
        } elseif (auth()->user()->isTeacher()) {
            $request->request->add([constants('USER_TYPES')['TEACHER'] => auth()->user()]);
        } elseif (auth()->user()->isStudent()) {
            $request->request->add([constants('USER_TYPES')['STUDENT'] => auth()->user()]);
        }
        $request->request->add(['user_type' => auth()->user()->user_type]);
        return $this;
    }
}
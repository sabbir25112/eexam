<?php namespace TSC\Repositories;

abstract class BaseRepository
{

    public $model;

    public function create(array $data)
    {
        return $this->model->firstOrCreate($data);
    }
}

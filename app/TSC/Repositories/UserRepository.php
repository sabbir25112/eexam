<?php namespace TSC\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{
    public function __construct(StudentRepository $studentRepository)
    {
        $this->model = new User();       
        $this->studentRepository = $studentRepository;
    }
}

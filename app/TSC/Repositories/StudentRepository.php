<?php namespace TSC\Repositories;

use App\Models\Student;

class StudentRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new Student();  
    }

    public function getAllWithSubject()
    {
        dd($this->model->paginate(5));
    }
}

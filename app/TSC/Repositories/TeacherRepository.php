<?php namespace TSC\Repositories;

use App\Models\Teacher;
use TSC\Repositories\UserRepository;
use \DB;
class TeacherRepository extends BaseRepository
{   
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->model = new Teacher();
        $this->userRepository = $userRepository;
    }

    public function create(array $data)
    {
        $data["user_type"] = constants('USER_TYPES')['TEACHER'];
        DB::transaction(function () use ($data, &$teacher){
            $user = $this->userRepository->create($data);
            $teacher = $user->teacher()->save($this->model);
        });

        return $teacher->load('user');
    }

    public function getAllWithUser()
    {
        return Teacher::with(['user', 'subjectTeacher'])->paginate(5);
    }

    public function findWithUser($teacher_id)
    {
        return $this->model->with('user')->find($teacher_id);
    }

    public function update($teacher, array $data)
    {
        return $teacher->user->update($data);
    }
}

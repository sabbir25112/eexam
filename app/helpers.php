<?php 

if (!function_exists('constants')) {

    function constants($key)
    {   
        return config("constants.$key");
    }
}

if (!function_exists('studentView')) {

    function studentView($key, $data = [])
    {
        return view("student.$key", $data);
    }
}

if (!function_exists('adminView')) {

    function adminView($key, $data = [])
    {
        return view("admin.$key", $data);
    }
}

if (!function_exists('teacherView')) {

    function teacherView($key, $data = [])
    {
        return view("teacher.$key", $data);
    }
}
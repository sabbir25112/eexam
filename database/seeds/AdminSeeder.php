<?php use Illuminate\Database\Seeder;
use App\Models\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'admin@tsc.test',
            'password' => bcrypt('123456'),
            'user_type' => 'admin'
        ]);
    }
}

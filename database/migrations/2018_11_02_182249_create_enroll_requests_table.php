<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnrollRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enroll_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('subject_teacher_id');
            $table->foreign('subject_teacher_id')
                ->references('id')->on('subject_teacher')
                ->onDelete('cascade');
            $table->unsignedInteger('student_id');
            $table->foreign('student_id')
                ->references('id')->on('students')
                ->onDelete('cascade');
            $table->enum('status', array_keys(constants('ENROLL_STATUS')))
                ->default(constants('ENROLL_STATUS')['Pending']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enroll_requests');
    }
}

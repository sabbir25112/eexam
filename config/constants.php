<?php 

return [
    'USER_TYPES' => [
        'TEACHER' => 'teacher',
        'STUDENT' => 'student',
        'ADMIN' => 'admin'
    ],

    'ENROLL_STATUS' => [
        'Pending' => 'Pending',
        'Accepted' => 'Accepted',
        'Rejected' => 'Rejected'
    ]
];
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'web', 'prefix' => '/'], function () {
    Auth::routes();
    Route::group(['middleware' => 'auth'], function() {
        Route::get('home', 'HomeController@index')->name('home');
        Route::resource('subject', 'SubjectController');
        Route::get('subject/student-list/{subject}', 'SubjectController@studentList')->name('subject.student-list');
        Route::resource('enroll-request', 'EnrollRequestController');
        Route::resource('exam', 'ExamController');
        Route::get('exam/change-status/{exam}', 'ExamController@changeStatus')->name('exam.change-status');
        Route::resource('notice', 'NoticeController');
        Route::resource('student', 'StudentController');
    });

    Route::group(['middleware' => 'admin_auth'], function () {
        Route::resource('teacher', 'TeacherController');
        Route::post('subject/{subject}/assign-teacher', 'SubjectController@assignTeacher');
    });

    Route::group(['middleware' => 'student_auth'], function () {
        Route::get('unenrolled-subjects', 'SubjectController@unenrolledSubjects')->name('unenrolled-subjects');
        Route::get('take-exam/{exam}', 'ExamController@takeExam')->name('exam.take-exam');
        Route::post('store-answer/{exam}', 'ExamController@storeAnswer')->name('exam.store-answer');
    });

    Route::group(['middleware' => 'teacher_auth'], function () {
        Route::resource('exam-question', 'ExamQuestionController');
        Route::get('exam/{exam}/student', 'ExamController@student')->name('exam.show-student');
    });
});

@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Student Details</h3>
                </div>

                <div class="panel-body">
                    <h4>Name: {{ $student->user->name }}</h4>
                    <h4>Student ID: {{ $student->user->student_id }}</h4>
                    <h4>Email: <code>{{ $student->user->email }}</code></h4>
                    <h4>Number of Subject: <code>{{ $student->subjectStudent->count() }}</code></h4>
                    <h4>Number of Exam: <code>{{ $student->examStudent->count() }}</code></h4>
                </div>
            </div>
        </div>

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Subjects</h3>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>ID.</th>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Teacher Name</th>
                            <th>Teacher Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($student->subjectStudent as $subjectStudent)
                        <tr>
                            <td>{{$subjectStudent->subjectTeacher->subject->id}}</td>
                            <td>{{$subjectStudent->subjectTeacher->subject->name}}</td>
                            <td>{{$subjectStudent->subjectTeacher->subject->code}}</td>
                            <td>{{$subjectStudent->subjectTeacher->teacher->user->name}}</td>
                            <td>{{$subjectStudent->subjectTeacher->teacher->user->email}}</td>
                        </tr>
                        @empty
                        <tr style="text-align:center;">
                            <td colspan=5><h4>No Subject Available</h4></td>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Exams</h3>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>ID.</th>
                            <th>Name</th>
                            <th>Subject</th>
                            <th>Mark</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($student->examStudent as $examStudent)
                        <tr>
                            <td>{{$examStudent->exam->id}}</td>
                            <td>{{$examStudent->exam->name}}</td>
                            <td>{{$examStudent->exam->subjectTeacher->subject->name}}</td>
                            <td>{{$examStudent->exam->calculateMark($student)}}</td>
                        </tr>
                        @empty
                        <tr style="text-align:center;">
                            <td colspan=4><h4>No Exam Available</h4></td>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
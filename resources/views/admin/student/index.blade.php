@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Student List</h3>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>ID.</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Student ID</th>
                            <th>Number of Subject</th>
                            <th>Number of Exams</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($students as $student)
                        <tr>
                            <td>{{$student->id}}</td>
                            <td>{{$student->user->name}}</td>
                            <td>{{$student->user->email}}</td>
                            <td>{{$student->user->student_id}}</td>
                            <td>{{$student->subject_student_count}}</td>
                            <td>{{$student->exam_student_count}}</td>
                            <td>
                                <a href="{{ route('student.show', $student->id) }}" class="btn btn-success">Detail</a>
                            </td>
                        </tr>
                        @empty
                        <tr style="text-align:center;">
                            <td colspan=5><h4>No Student Available</h4></td>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {{ $students->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

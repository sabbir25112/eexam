@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">{{$teacher->user->name}}</h3>
                </div>

                <div class="panel-body">
                    <h4>Name: {{$teacher->user->name}}</h4>
                    <h4>Email: {{$teacher->user->email}}</h4>
                    <h4>Number of Subject: {{count($teacher->subjectTeacher)}}</h4>
                </div>
            </div>
        </div>

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Subjects</h3>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Number of Students</th>
                            <th>Number of Exams</th>
                            <th>Pending Requests</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($teacher->subjectTeacher as $subject)
                        <tr>
                            <td>{{$subject->subject->name}}</td>
                            <td>{{$subject->subject->code}}</td>
                            <td>{{count($subject->subjectStudent)}}</td>
                            <td>{{count($subject->exams)}}</td>
                            <td>{{count($subject->enroll_request)}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
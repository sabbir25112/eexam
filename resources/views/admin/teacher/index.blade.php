@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Teacher List</h3>
                    <a href="{{ route('teacher.create') }}" class="btn btn-primary pull-right">Add</a>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Subject</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($teachers as $teacher)
                        <?php $user = $teacher->user ?>
                        <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{count($teacher->subjectTeacher)}}</td>
                            <td>
                                <a href="{{ route('teacher.show', $teacher->id) }}" class="btn btn-success">Detail</a>
                                <a href="{{ route('teacher.edit', $teacher->id) }}" class="btn btn-default">Edit</a>
                            </td>
                        </tr>
                        @empty
                        <tr style="text-align:center;">
                            <td colspan=4><h4>No Teacher Available</h4></td>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {{ $teachers->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Teacher Edit</h3>
                </div>

                <div class="panel-body">
                    <form action="{{ route('teacher.update', $teacher->id) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="form-group">
                            <label>Name:</label>
                            <input type="input" class="form-control" name="name" value="{{ old('name') ? : $teacher->user->name }}" required>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Email address:</label>
                            <input type="email" class="form-control" name="email" value="{{ old('email') ? : $teacher->user->email }}" required>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Password:</label>
                            <input type="password" class="form-control" name="password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Password Again:</label>
                            <input type="password" class="form-control" name="password_confirmation">
                        </div>

                        <input type="submit" class="btn btn-primary" value="Add Teacher">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<table class="table table-bordered table-hover table-striped">
    <thead>
    <tr>
        <th>ID.</th>
        <th>Name</th>
        <th>Email</th>
        <th>Number of Students</th>
    </tr>
    </thead>
    <tbody id="assigned_teacher_table">
    @forelse($teachers as $teacher)
    <?php $user = $teacher->user ?>
    <tr>
        <td>{{$teacher->id}}</td>
        <td>{{$user->name}}</td>
        <td>{{$user->email}}</td>
        <td>{{$teacher->subject_student_count}}</td>
    </tr>
    @empty
    <tr style="text-align:center;">
        <td colspan=5><h4>No Teacher Available</h4></td>
    </tr>
    @endforelse
    </tbody>
</table>
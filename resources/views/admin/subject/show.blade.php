@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Subject Details</h3>
                </div>

                <div class="panel-body">
                    <h4>Name: {{ $subject->name }}</h4>
                    <h4>Code: <code>{{ $subject->code }}</code></h4>
                </div>
            </div>
        </div>

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Assigned Teachers to {{ $subject->name }}</h3>
                </div>

                <div class="panel-body">
                    @include('admin.subject._teacher_table', ['teachers' => $subject->teacher])
                </div>
            </div>
        </div>

        @if ($teachers->count())
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Assigne Teacher to {{ $subject->name }}</h3>
                </div>

                <div class="panel-body">
                    @include('admin.subject._assign_teacher', ['teachers' => $teachers, 'subject' => $subject])
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection

@section('scripts')
<script>
    var teachers = JSON.parse('{!! addslashes(json_encode($teachers)) !!}');
    var assigned_teacher_ids = JSON.parse('{!! addslashes(json_encode($subject->teacher->pluck('id')->toArray())) !!}');
</script>
@stack('other_scripts')
@endsection
@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Subject List</h3>
                    <a href="{{ route('subject.create') }}" class="btn btn-primary pull-right">Add</a>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>ID.</th>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Number of Teacher</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($subjects as $subject)
                        <tr>
                            <td>{{$subject->id}}</td>
                            <td>{{$subject->name}}</td>
                            <td>{{$subject->code}}</td>
                            <td>{{$subject->teacher_count}}</td>
                            <td>
                                <a href="{{ route('subject.show', $subject->id) }}" class="btn btn-success">Detail</a>
                                <a href="{{ route('subject.edit', $subject->id) }}" class="btn btn-default">Edit</a>
                            </td>
                        </tr>
                        @empty
                        <tr style="text-align:center;">
                            <td colspan=5><h4>No Subject Available</h4></td>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {{ $subjects->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

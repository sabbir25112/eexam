@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Subject Create</h3>
                </div>

                <div class="panel-body">
                    <form action="{{ route('subject.store') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Subject Name:</label>
                            <input type="input" class="form-control" name="name" value="{{ old('name') }}" required>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Subject Code:</label>
                            <input type="input" class="form-control" name="code" value="{{ old('code') }}" required>
                            @if ($errors->has('code'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('code') }}</strong>
                                </span>
                            @endif
                        </div>

                        <input type="submit" class="btn btn-primary" value="Add Subject">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

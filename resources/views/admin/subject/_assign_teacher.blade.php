<div class="col-md-8">
    <div class="form-group">
        <label>Subject Name:</label>
        <select id="selected_teacher" class="form-control">
            
        </select>
        @if ($errors->has('name'))
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="col-md-4">
    <button class="btn btn-primary" style="margin-top:12%;" id="teacher_assign_button">Assign</button>
</div>
@push('other_scripts')
<script>
    var teacher_assign_url = '{{ env("APP_URL")."/subject/$subject->id/assign-teacher" }}';
    var csrf_token = '{{ csrf_token() }}';

    function updateTeacherDropdown()
    {
        $('#selected_teacher').html('');
        teachers.map((teacher) => {
            $('#selected_teacher').append(`<option value="${teacher.id}">${teacher.user.name}</option>`);
        });
    }
    updateTeacherDropdown();

    $('#teacher_assign_button').click(function() {
        let selected_teacher_id = $('#selected_teacher').val();
        let selected_teacher = [selected_teacher_id].concat(assigned_teacher_ids);
        jQuery.ajax({
            type:"post",
            dataType:"json",
            url: teacher_assign_url,
            data: {_token: csrf_token, teacher: selected_teacher},
            success: function(data) {
                if (data) {
                    assigned_teacher_ids.push(selected_teacher_id);
                    teachers = teachers.filter(function (teacher) {
                        return teacher.id != selected_teacher_id;
                    });
                    updateTeacherDropdown();
                }
            },
            error: function(data) {
                
            },
        });
    });
</script>
@endpush
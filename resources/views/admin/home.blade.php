@extends('layouts.student.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Admin Dashboard</div>

                <div class="panel-body">
                    <div class="row">
                        <a href="{{ route('teacher.index') }}" class="btn btn-primary">Teachers</a>
                        <a href="{{ route('student.index') }}" class="btn btn-primary">Students</a>
                        <a href="{{ route('subject.index') }}" class="btn btn-primary">Subject</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

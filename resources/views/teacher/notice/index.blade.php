@extends('layouts.teacher.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Notice List</h3>
                    <a href="{{route('notice.create')}}" class="btn btn-primary">Add Notice</a>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped table-responsive">
                        <thead>
                        <tr>
                            <th>ID.</th>
                            <th>Subject Name</th>
                            <th>Subject Code</th>
                            <th>Title</th>
                            <th>Message</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($notices as $notice)
                        <tr>
                            <td>{{$notice->id}}</td>
                            <td>{{$notice->subjectTeacher->subject->name}}</td>
                            <td>{{$notice->subjectTeacher->subject->code}}</td>
                            <td>{{$notice->title}}</td>
                            <td>{{$notice->message}}</td>
                            <td>
                                <a href="{{route('notice.edit', $notice->id)}}" class="btn btn-primary">Edit</a>
                            </td>
                        </tr>
                        @empty
                        <tr style="text-align:center;">
                            <td colspan=5><h4>No Notice Available</h4></td>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

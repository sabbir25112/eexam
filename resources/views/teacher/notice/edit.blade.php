@extends('layouts.teacher.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Notice Edit</h3>
                </div>

                <div class="panel-body">
                    <form action="{{ route('notice.update', $notice->id) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" name="title" value="{{ old('title') ?: $notice->title }}" required>
                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Subject:</label>
                            <select name="subject_teacher_id" class="form-control">
                                @foreach($teacher->subjectTeacher as $subject_teacher)
                                <option value="{{$subject_teacher->id}}" {{ $subject_teacher->id == $notice->subject_teacher_id ? 'selected' : '' }}>
                                    {{$subject_teacher->subject->name}}
                                </option>
                                @endforeach
                            </select>
                            @if ($errors->has('subject_teacher_id'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('subject_teacher_id') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Message</label>
                            <textarea class="form-control" rows="5" name="message">{{$notice->message}}</textarea>
                            @if ($errors->has('message'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('message') }}</strong>
                                </span>
                            @endif
                        </div>

                        <input type="submit" class="btn btn-primary" value="Update Notice">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.student.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Students of {{$subject->subject->name}}</h3>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Student Name</th>
                            <th>Student Email</th>
                            <th>Student ID</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($subject->subjectStudent as $subject_student)
                        <tr>
                            <td>{{$subject_student->student->id}}</td>
                            <td>{{$subject_student->student->user->name}}</td>
                            <td>{{$subject_student->student->user->email}}</td>
                            <td>{{$subject_student->student->user->student_id}}</td>
                        </tr>
                        @empty
                        <tr style="text-align:center;">
                            <td colspan=4><h4>No Student Available</h4></td>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
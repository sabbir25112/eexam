@extends('layouts.teacher.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Subject List</h3>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>ID.</th>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Number of Students</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($subjects as $subject)
                        <tr>
                            <td>{{$subject->subject->id}}</td>
                            <td>{{$subject->subject->name}}</td>
                            <td>{{$subject->subject->code}}</td>
                            <td>{{$subject->subject_student_count}}</td>
                            <td>
                            <a href="{{route('subject.student-list', $subject->id)}}" class="btn btn-primary">Student List</a>
                            </td>
                        </tr>
                        @empty
                        <tr style="text-align:center;">
                            <td colspan=5><h4>No Subject Available</h4></td>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

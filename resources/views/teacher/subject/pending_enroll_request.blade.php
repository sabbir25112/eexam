@extends('layouts.student.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Pending Enroll Request</h3>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Student Name</th>
                            <th>Student Email</th>
                            <th>Student ID</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($pending_requests as $pending_request)
                        <tr>
                            <td>{{$pending_request->subjectTeacher->subject->name}}</td>
                            <td>{{$pending_request->subjectTeacher->subject->code}}</td>
                            <td>{{$pending_request->student->user->name}}</td>
                            <td>{{$pending_request->student->user->email}}</td>
                            <td>{{$pending_request->student->user->student_id}}</td>
                            <td>
                                <button class="btn btn-success status-change" data-status="{{constants('ENROLL_STATUS')['Accepted']}}" data-id="{{$pending_request->id}}">Accept</button>
                                <button class="btn btn-danger status-change" data-status="{{constants('ENROLL_STATUS')['Rejected']}}" data-id="{{$pending_request->id}}">Reject</button>
                            </td>
                        </tr>
                        @empty
                        <tr style="text-align:center;">
                            <td colspan=6><h4>No Subject Available</h4></td>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {{ $pending_requests->links()}}
                </div>
            </div>
        </div>
    </div>
</div>

<form method="POST" id="enroll-status-change-form">
    {{csrf_field()}}
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="status" id="enroll-status-input">
</form>
@endsection

@section('scripts')
<script>
    var url = '{{ url("enroll-request") }}';

    $('.status-change').click(function() {
        let status = $(this).data('status');
        let id = $(this).data('id');

        $('#enroll-status-input').val(status);
        $('#enroll-status-change-form').attr('action', url + '/' + id);
        $('#enroll-status-change-form').submit();
    });
</script>
@endsection
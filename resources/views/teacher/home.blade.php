@extends('layouts.teacher.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="{{route('subject.index')}}" class="btn btn-primary">Subjects</a>
                    <a href="{{route('enroll-request.index')}}" class="btn btn-primary">Pending Request</a>
                    <a href="{{route('exam.index')}}" class="btn btn-primary">Exam</a>
                    <a href="{{route('notice.index')}}" class="btn btn-primary">Notice</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

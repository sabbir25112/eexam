@extends('layouts.teacher.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Edit Question</h3>
                </div>

                <div class="panel-body">
                    <form action="{{ route('exam-question.update', ['examQuestion' => $examQuestion->id]) }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group">
                            <label>Question</label>
                            <input type="text" class="form-control" name="question" value="{{ old('question') ?: $examQuestion->question_statement }}" required placeholder="Add Question Statement">
                            @if ($errors->has('question'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('question') }}</strong>
                                </span>
                            @endif
                        </div>

                        <?php 
                            $alphabets = range('A', 'Z');
                            $image_url_prefix = URL::to('/img/exam_questions/');
                        ?>
                        @foreach($examQuestion->options as $count => $option)
                        <div class="form-group">
                            <label>Option {{$alphabets[$count]}}</label>
                            <input type="text" class="form-control" name="option_{{$count+1}}" value="{{ old('option_'.$count+1) ?: $option->option_statement }}" required>
                            @if ($errors->has('option_'. $count+1))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('option_'. $count+1) }}</strong>
                                </span>
                            @endif
                            <br>

                            @if ($option->option_image)
                            <img src="{{$image_url_prefix . '/' .$option->option_image}}" 
                                 alt="Option Image" style="height:150px;"><br>
                            @endif

                            <label>Image for Option {{$alphabets[$count]}} (optional)</label>
                            <input type="file" class="form-control" name="option_image_{{$count+1}}">
                            @if ($errors->has('option_image_'.$count+1))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('option_image_'.$count+1) }}</strong>
                                </span>
                            @endif
                        </div>
                        @endforeach

                        <div class="form-group">
                            <label>Answer</label>
                            <select name="answer" class="form-control">
                                @foreach($examQuestion->options as $count => $option)
                                <option value="{{$count+1}}" {{ $option->is_answer ? 'selected' : ''}}>{{$alphabets[$count]}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('answer'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('answer') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Mark</label>
                            <input type="number" class="form-control" min="1" name="mark" value="{{ old('mark') ?: $examQuestion->mark }}" required>
                            @if ($errors->has('mark'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('mark') }}</strong>
                                </span>
                            @endif
                        </div>
                        <input type="submit" class="btn btn-primary" value="Edit Question">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

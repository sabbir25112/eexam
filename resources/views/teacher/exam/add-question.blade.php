@extends('layouts.teacher.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Add Question to {{ $exam->name}}</h3>
                </div>

                <div class="panel-body">
                    <form action="{{ route('exam-question.store') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label>Question</label>
                            <input type="text" class="form-control" name="question" value="{{ old('question') }}" required placeholder="Add Question Statement">
                            @if ($errors->has('question'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('question') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                        <div class="form-group">
                            <label>Option A</label>
                            <input type="text" class="form-control" name="option_1" value="{{ old('option_1') }}" required>
                            @if ($errors->has('option_1'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('option_1') }}</strong>
                                </span>
                            @endif
                            <br>
                            <label>Image for Option A (optional)</label>
                            <input type="file" class="form-control" name="option_image_1">
                            @if ($errors->has('option_image_1'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('option_image_1') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Option B</label>
                            <input type="text" class="form-control" name="option_2" value="{{ old('option_2') }}" required>
                            @if ($errors->has('option_2'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('option_2') }}</strong>
                                </span>
                            @endif
                            <br>
                            <label>Image for Option B (optional)</label>
                            <input type="file" class="form-control" name="option_image_2">
                            @if ($errors->has('option_image_2'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('option_image_2') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Option C</label>
                            <input type="text" class="form-control" name="option_3" value="{{ old('option_3') }}" required>
                            @if ($errors->has('option_3'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('option_3') }}</strong>
                                </span>
                            @endif
                            <br>
                            <label>Image for Option C (optional)</label>
                            <input type="file" class="form-control" name="option_image_3">
                            @if ($errors->has('option_image_3'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('option_image_3') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Option D</label>
                            <input type="text" class="form-control" name="option_4" value="{{ old('option_4') }}" required>
                            @if ($errors->has('option_4'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('option_4') }}</strong>
                                </span>
                            @endif
                            <br>
                            <label>Image for Option D (optional)</label>
                            <input type="file" class="form-control" name="option_image_4">
                            @if ($errors->has('option_image_4'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('option_image_4') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Answer</label>
                            <select name="answer" class="form-control">
                                <option value="1">A</option>
                                <option value="2">B</option>
                                <option value="3">C</option>
                                <option value="4">D</option>
                            </select>
                            @if ($errors->has('answer'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('answer') }}</strong>
                                </span>
                            @endif
                        </div>
                        <input type="hidden" name="exam_id" value="{{request('exam_id')}}">
                        <div class="form-group">
                            <label>Mark</label>
                            <input type="number" class="form-control" min="1" name="mark" value="{{ old('mark') || 1 }}" required>
                            @if ($errors->has('mark'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('mark') }}</strong>
                                </span>
                            @endif
                        </div>
                        <input type="submit" class="btn btn-primary" value="Add Question">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

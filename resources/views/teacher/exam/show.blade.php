@extends('layouts.teacher.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">{{$exam->name}}</h3>
                    @if (!$exam->is_publish)
                        <a  href="{{route('exam-question.create', ['exam_id' => $exam->id])}}" class="btn btn-primary pull-right">Add Question</a>
                    @endif
                    <a  href="{{route('exam.change-status', ['exam_id' => $exam->id])}}"
                        class="btn pull-right {{$exam->is_publish ? 'btn-danger' : 'btn-success'}}">
                        {{$exam->is_publish ? 'Unpublish' : 'Publish'}}
                    </a>
                </div>

                <div class="panel-body">
                    <h4>Name: {{ $exam->name }}</h4>
                    <h4>Time: <code>{{ $exam->time }} Min(s)</code></h4>
                    <h4>Subject: {{$exam->subjectTeacher->subject->name}}</h4>
                    <h4>Total Question: {{$exam->questions->count()}}</h4>
                    <h4>Total Marks: {{$exam->questions->sum('mark')}}</h4>
                    <h4>Published: <code>{{$exam->is_publish ? 'Yes' : 'No'}}</code></h4>
                </div>
            </div>
        </div>

        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Questions</h3>
                </div>

                <div class="panel-body">
                    <?php 
                        $alphabet = range('a', 'z'); 
                        $image_url_prefix = URL::to('/img/exam_questions/');
                    ?>
                    @foreach($exam->questions as $key => $question)
                        <h3>
                            {{$key + 1}}) {{$question->question_statement}} <span class="pull-right">{{$question->mark}}</span>
                            @if (!$exam->is_publish)
                            <a href="{{route('exam-question.edit', ['exam-qustion' => $question->id])}}" class="btn btn-primary pull-right">Edit</a>
                            @endif
                        </h3>
                        @foreach($question->options as $count => $option)
                            <h4 style="{{$option->is_answer ? 'color: green; font-weight:600;' : ''}}">{{$alphabet[$count]}}) {{$option->option_statement}}</h4> 
                            @if($option->option_image)
                            <img src="{{$image_url_prefix . '/' .$option->option_image}}" 
                                 alt="Option Image" style="height:150px;">
                            @endif
                        @endforeach
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.teacher.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Exam Create</h3>
                </div>

                <div class="panel-body">
                    <form action="{{ route('exam.store') }}" method="POST">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Subject:</label>
                            <select name="subject" class="form-control">
                                @foreach($subjects as $subject)
                                <option value="{{$subject->id}}">{{$subject->subject->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Time</label>
                            <input type="number" class="form-control" name="time" value="{{ old('time') }}" required>
                            @if ($errors->has('time'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('time') }}</strong>
                                </span>
                            @endif
                        </div>

                        <input type="submit" class="btn btn-primary" value="Add Exam">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

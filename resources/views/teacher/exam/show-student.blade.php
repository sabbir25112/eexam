@extends('layouts.teacher.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Student List for {{$exam->name}} </h3>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped table-responsive">
                        <thead>
                        <tr>
                            <th>ID.</th>
                            <th>Student Name</th>
                            <th>Student Email</th>
                            <th>Student ID</th>
                            <th>Mark</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($exam->students as $student)
                        <tr>
                            <td>{{$student->student->id}}</td>
                            <td>{{$student->student->user->name}}</td>
                            <td>{{$student->student->user->email}}</td>
                            <td>{{$student->student->user->student_id}}</td>
                            <td>{{$exam->calculateMark($student->student)}}</td>
                        </tr>
                        @empty
                        <tr style="text-align:center;">
                            <td colspan=5><h4>No Student Found</h4></td>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

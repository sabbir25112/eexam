@extends('layouts.teacher.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Exam List</h3>
                    <a href="{{route('exam.create')}}" class="btn btn-primary">Add Exam</a>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped table-responsive">
                        <thead>
                        <tr>
                            <th>ID.</th>
                            <th>Name</th>
                            <th>Subject Name</th>
                            <th>Subject Code</th>
                            <th>Publish</th>
                            <th>Time</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($exams as $exam)
                        <tr>
                            <td>{{$exam->id}}</td>
                            <td>{{$exam->name}}</td>
                            <td>{{$exam->subjectTeacher->subject->name}}</td>
                            <td>{{$exam->subjectTeacher->subject->code}}</td>
                            <td>{{$exam->is_publish ? 'Yes' : 'No'}}</td>
                            <td>{{$exam->time}}</td>
                            <td>
                                <a href="{{route('exam.change-status', ['exam_id' => $exam->id])}}"
                                    class="btn {{$exam->is_publish ? 'btn-danger' : 'btn-success'}}">
                                    {{$exam->is_publish ? 'Unpublish' : 'Publish'}}
                                </a>
                                <a href="{{route('exam.show', ['exam' => $exam->id])}}" class="btn btn-default">Details</a>
                                <a href="{{route('exam-question.create', ['exam_id' => $exam->id])}}" class="btn btn-success">Add Question</a>
                                <a href="{{route('exam.show-student', $exam->id)}}" class="btn btn-primary">Students</a>
                            </td>
                        </tr>
                        @empty
                        <tr style="text-align:center;">
                            <td colspan=5><h4>No Exam Available</h4></td>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

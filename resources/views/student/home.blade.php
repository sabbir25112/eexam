@extends('layouts.student.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="{{route('subject.index')}}" class="btn btn-primary">Subject</a>
                    <a href="{{route('exam.index')}}" class="btn btn-primary">Exams</a>
                    <a href="{{route('student.edit', auth()->user()->student->id)}}" class="btn btn-primary">Edit Profile</a>
                </div>
            </div>
        </div>

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Notice</div>

                <div class="panel-body">
                <table class="table table-bordered table-hover table-striped table-responsive">
                        <thead>
                        <tr>
                            <th>Subject Name</th>
                            <th>Subject Code</th>
                            <th>Title</th>
                            <th>Message</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($notices as $notice)
                        <tr>
                            <td>{{$notice->subjectTeacher->subject->name}}</td>
                            <td>{{$notice->subjectTeacher->subject->code}}</td>
                            <td>{{$notice->title}}</td>
                            <td>{{$notice->message}}</td>
                        </tr>
                        @empty
                        <tr style="text-align:center;">
                            <td colspan=5><h4>No Notice Available</h4></td>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

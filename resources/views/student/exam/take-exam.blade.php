@extends('layouts.student.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">{{$exam->name}}</h3>
                    <div id="timer" class="pull-right" style="color:green; font-weight:600;font-size:20px;">
                    </div>
                </div>

                <div class="panel-body">
                <?php 
                    $alphabet = range('a', 'z'); 
                    $image_url_prefix = URL::to('/img/exam_questions/');
                ?>
                <form action="{{route('exam.store-answer', $exam->id)}}" method="POST">
                {{ csrf_field()}}
                @foreach($exam->questions as $key => $question)
                    <h3>
                        {{$key + 1}} {{$question->question_statement}}
                    </h3>
                    @foreach($question->options as $count => $option)
                    <div class="col-md-6">
                        <label>
                            <input type="radio" name="question_answer_{{$question->id}}" value="{{$option->id}}">
                            {{$option->option_statement}}
                        </label><br>
                        
                        @if($option->option_image)
                        <img src="{{$image_url_prefix . '/' .$option->option_image}}" 
                                alt="Option Image" style="height:150px;"><br>
                        @endif
                    </div>
                    @endforeach
                @endforeach
                <input type="hidden" name="exam_student" value="{{$exam_student->id}}">
                <input type="submit" class="btn btn-success" value="Submit">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('js/timer.js') }}"></script>
<script>
// Set the date we're counting down to
var countDownDate = new Date('{!! $exam_end_time !!}').getTime();
var questions = JSON.parse('{!! json_encode($exam->questions) !!}');

</script>
@endsection
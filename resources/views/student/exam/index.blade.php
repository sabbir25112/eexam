@extends('layouts.student.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Exam List</h3>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped table-responsive">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Subject Name</th>
                            <th>Subject Code</th>
                            <th>Teacher Name</th>
                            <th>Time</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($exams as $exam)
                        <tr>
                            <td>{{$exam->name}}</td>
                            <td>{{$exam->subjectTeacher->subject->name}}</td>
                            <td>{{$exam->subjectTeacher->subject->code}}</td>
                            <td>{{$exam->subjectTeacher->teacher->user->name}}</td>
                            <td>{{$exam->time}}</td>
                            <td>
                                <a href="{{route('exam.show', $exam->id)}}" class="btn btn-primary">Details</a>
                            </td>
                        </tr>
                        @empty
                        <tr style="text-align:center;">
                            <td colspan=5><h4>No Exam Available</h4></td>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

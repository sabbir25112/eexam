@extends('layouts.student.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">{{$exam->name}}</h3>
                    @if ($exam->isEligibleNow($student))
                        <a href="{{route('exam.take-exam', $exam->id)}}" class="btn btn-success pull-right">Start Exam</a>
                    @endif
                </div>

                <div class="panel-body">
                    <h4>Name: {{ $exam->name }}</h4>
                    <h4>Time: <code>{{ $exam->time }} Min(s)</code></h4>
                    <h4>Subject: {{$exam->subjectTeacher->subject->name}}</h4>
                    <h4>Total Question: {{$exam->questions->count()}}</h4>
                    <h4>Total Marks: {{$exam->questions->sum('mark')}}</h4>
                    @if (!$exam->isEligibleNow($student))
                    <h4>My Marks: {{$exam->calculateMark($student)}}</h4>
                    @endif
                </div>
            </div>
        </div>

        @if (!$exam->isEligibleNow($student))
        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Questions</h3>
                </div>

                <div class="panel-body">
                    <?php 
                        $alphabet = range('a', 'z'); 
                        $image_url_prefix = URL::to('/img/exam_questions/');
                    ?>
                    @foreach($exam->questions as $key => $question)
                        <h3>
                            {{$key + 1}}) {{$question->question_statement}} <span class="pull-right">{{$question->mark}}</span>
                        </h3>
                        @foreach($question->options as $count => $option)
                            <h4 style="{{$option->is_answer ? 'color: green; font-weight:600;' : ''}}">{{$alphabet[$count]}}) {{$option->option_statement}}</h4> 
                            @if($option->option_image)
                            <img src="{{$image_url_prefix . '/' .$option->option_image}}" 
                                 alt="Option Image" style="height:150px;">
                            @endif
                        @endforeach
                    @endforeach
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
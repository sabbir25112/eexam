@extends('layouts.student.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Enroll Subject </h3>
                </div>

                <div class="panel-body">
                    <form action="{{route('enroll-request.store')}}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Select Subject:</label>
                            <select name="subject" id="subjectDropdown" class="form-control">
                                @foreach($subjects as $subject)
                                <option value="{{$subject->id}}">{{"$subject->name ($subject->code)"}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('subject'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('subject') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Select Subject:</label>
                            <select name="subject_teacher" id="teacherDropdown" class="form-control">

                            </select>
                            @if ($errors->has('subject_teacher'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('subject_teacher') }}</strong>
                                </span>
                            @endif
                        </div>
                        <input type="submit" class="btn btn-primary" value="Enroll">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var subjects = JSON.parse('{!! addslashes(json_encode($subjects)) !!}');

    $('#subjectDropdown').change(function() {
        let subject_id = $(this).val();
        updateTeacherDropdown(subject_id);
    });

    function updateTeacherDropdown(subject_id) 
    {
        $('#teacherDropdown').html('');

        let subject = subjects.find(subject => subject.id == subject_id);
        subject.subject_teacher.map((pivot) => {
            let teacher = pivot.teacher.user;
            $('#teacherDropdown').append(`<option value="${pivot.id}">${teacher.name}</option>`)
        });
    }
    if (subjects.length) updateTeacherDropdown(subjects[0].id);
</script>
@endsection
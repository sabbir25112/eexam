@extends('layouts.student.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Subject List</h3>
                    <a href="{{route('unenrolled-subjects')}}" class="btn btn-primary">Enroll Subject</a>
                    <a href="{{route('enroll-request.index')}}" class="btn btn-primary">Pending Requests</a>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Teacher Name</th>
                            <th>Teacher Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($subjects as $subject)
                        <tr>
                            <td>{{$subject->subjectTeacher->subject->name}}</td>
                            <td>{{$subject->subjectTeacher->subject->code}}</td>
                            <td>{{$subject->subjectTeacher->teacher->user->name}}</td>
                            <td>{{$subject->subjectTeacher->teacher->user->email}}</td>
                        </tr>
                        @empty
                        <tr style="text-align:center;">
                            <td colspan=5><h4>No Subject Available</h4></td>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {{ $subjects->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

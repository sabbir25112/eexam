@extends('layouts.student.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="display:inline;">Pending Enroll Request</h3>
                    <a href="{{route('unenrolled-subjects')}}" class="btn btn-primary">Enroll Subject</a>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Teacher Name</th>
                            <th>Teacher Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($pending_requests as $pending_request)
                        <tr>
                            <td>{{$pending_request->subjectTeacher->subject->name}}</td>
                            <td>{{$pending_request->subjectTeacher->subject->code}}</td>
                            <td>{{$pending_request->subjectTeacher->teacher->user->name}}</td>
                            <td>{{$pending_request->subjectTeacher->teacher->user->email}}</td>
                        </tr>
                        @empty
                        <tr style="text-align:center;">
                            <td colspan=5><h4>No Subject Available</h4></td>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {{ $pending_requests->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
